#include "unity.h"

#include "Mockmessage.h"

#include "message_processor.h"

// This only tests if we process at most 3 messages
void test_process_3_messages(void) {
  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  // Third time when message_read() is called, we will return false to break the loop
  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  // Since we did not return a message that starts with '$' this should return false
  TEST_ASSERT_FALSE(message_processor());
}

void test_process_message_with_dollar_sign(void) {
  message_s message_to_read;
  message_to_read.data[0] = '$';

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  // Third time when message_read() is called, we will return false to break the loop
  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&message_to_read);
  // Since we did not return a message that starts with '$' this should return false
  TEST_ASSERT_TRUE(message_processor());
}

void test_process_messages_without_any_dollar_sign(void) {
  message_s message_to_read;
  message_to_read.data[0] = 's';

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  // Third time when message_read() is called, we will return false to break the loop
  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&message_to_read);
  // Since we did not return a message that starts with '$' this should return false
  TEST_ASSERT_TRUE(message_processor());
}

void test_process_messages_with_hash_sign(void) {
  message_s message_to_read;
  message_to_read.data[0] = '#';

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();

  // Third time when message_read() is called, we will return false to break the loop
  message__read_ExpectAndReturn(NULL, true);
  message__read_IgnoreArg_message_to_read();
  message__read_ReturnThruPtr_message_to_read(&message_to_read);
  // Since we did not return a message that starts with '$' this should return false
  TEST_ASSERT_TRUE(message_processor());
}

void test_process_messages_with_stubWithCallback(void) {

  message__read_StubWithCallback(message__read_Stub);
  TEST_ASSERT_FALSE(message_processor());
}