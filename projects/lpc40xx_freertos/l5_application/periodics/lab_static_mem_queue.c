#include "lab_queue.h"

uint8_t max_queue_size;
// uint8_t queue_pop_memory[100];

static queue_s queue;

bool queue__init(queue_s *queue, void *static_memory_for_queue, size_t static_memory_size_in_bytes) {
  if ((NULL != queue) && (NULL != static_memory_for_queue)) {
    queue->head = 0;
    queue->tail = 0;
    queue->item = 0;
    queue->static_memory_for_queue = (uint8_t *)static_memory_for_queue;
    queue->static_memory_size_in_bytes = static_memory_size_in_bytes;
    return true;
  } else {
    return false;
  }
}

// This should initialize all members of queue_s
// void queue__init(queue_s *queue);

/// @returns false if the queue is full
bool queue__push(queue_s *queue, uint8_t push_value) {
  if (queue->item != max_queue_size) {
    queue->queue_memory[queue->item] = push_value;
    queue->item = queue->item + 1;
    return true;
  } else
    return false;
}

/// @returns false if the queue was empty
bool queue__pop(queue_s *queue, uint8_t *pop_value) {
  static size_t index;
  if (queue->item == 0) {
    return false;
  } else {
    *pop_value = queue->queue_memory[index];
    queue->queue_memory[index] = 0;
    index++;
    queue->item = queue->item - 1;
    if (queue->item == 0)
      index = 0;
    return true;
  }
}

size_t queue__get_item_count(const queue_s *queue) { return queue->item; }
/*size_t queue__get_item_count(const queue_s *queue) {
  size_t count = 0, index = 0;
  while (index < 100) {
    if (NULL != queue->queue_memory[index] != 0) {
      count++;
    }
    index++;
  }
  return count;
}*/
