#include <stdio.h>
#include <string.h>

#include "lab_queue.h"
#include "unity.h"

queue_s queue;

void test_comprehensive(void) {
  const size_t max_queue_size = 100; // Change if needed
  for (size_t item = 0; item < max_queue_size; item++) {
    // if (item == 0)
    // queue__init(&queue);
    const uint8_t item_pushed = (uint8_t)item;
    TEST_ASSERT_TRUE(queue__push(&queue, item_pushed));
    TEST_ASSERT_EQUAL(item + 1, queue__get_item_count(&queue));
  }

  // Should not be able to push anymore
  TEST_ASSERT_FALSE(queue__push(&queue, 123));
  TEST_ASSERT_EQUAL(max_queue_size, queue__get_item_count(&queue));

  // Pull and verify the FIFO order
  for (size_t item = 0; item < max_queue_size; item++) {
    uint8_t popped_value = 0;
    TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
    TEST_ASSERT_EQUAL((uint8_t)item, popped_value);
  }

  // Test wrap-around case
  const uint8_t pushed_value = 123;
  TEST_ASSERT_TRUE(queue__push(&queue, pushed_value));
  uint8_t popped_value = 0;
  TEST_ASSERT_TRUE(queue__pop(&queue, &popped_value));
  TEST_ASSERT_EQUAL(pushed_value, popped_value);

  TEST_ASSERT_EQUAL(0, queue__get_item_count(&queue));
  TEST_ASSERT_FALSE(queue__pop(&queue, &popped_value));
}